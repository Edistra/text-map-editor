import {Component, OnInit, SimpleChanges} from '@angular/core';
import {FormGroup, FormArray, Validators, FormBuilder} from '@angular/forms';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-map-editor',
  templateUrl: './map-editor.component.html',
  styleUrls: ['./map-editor.component.css']
})
export class MapEditorComponent implements OnInit {
  mapForm = this.fb.group({
    name: ['level0X', Validators.required],
    width: [10, Validators.required],
    layers: this.fb.array([
      this.getLayerGroup()
    ])
  });

  constructor(private fb: FormBuilder, private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.layers.valueChanges
      .subscribe(() => this.updateLayers());
    this.mapForm.get('width').valueChanges
      .subscribe(() => this.updateLayers());
  }

  getLayerGroup() {
    return this.fb.group({
      walls: this.fb.control(('x'.repeat(10) + '\n').repeat(5)),
      entities: this.fb.control('')
    });
  }

  get jsonMap() {
    return JSON.stringify(this.layers.value.map(layer =>
      Object.entries(layer).reduce((accumulator, subLayer) => ({
        ...accumulator,
        [subLayer[0]]: (subLayer[1] as string).split('\n')
      }), {})
    ));
  }

  get downloadUrl() {
    return this.sanitizer.bypassSecurityTrustUrl('data:text/json;charset=UTF-8,' + encodeURIComponent(this.jsonMap));
  }

  get layers(): FormArray {
    return this.mapForm.get('layers') as FormArray;
  }

  updateLayers() {
    this.layers.controls.forEach((layer: FormGroup) => {
      for (const subLayerKey of Object.keys(layer.controls)) {
        const subLayer = layer.controls[subLayerKey];
        subLayer.setValue(
          subLayer.value.length ?
            subLayer.value.replace(/\n/g, '').match(new RegExp('.{1,' + this.mapForm.get('width').value + '}', 'g')).join('\n') : '',
          {
            emitEvent: false
          }
        );
      }
    });
    localStorage.setItem('textmap', this.jsonMap);
  }

  addLayer() {
    this.layers.push(this.getLayerGroup());
  }

  preview() {
    const blob = new Blob([this.jsonMap], {type: 'text/json'});
    const url = window.URL.createObjectURL(blob);
    window.open(url);
  }
}
