import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { MapEditorComponent } from './map-editor/map-editor.component';
import { KeysPipePipe } from './keys-pipe.pipe';

@NgModule({
  declarations: [
    AppComponent,
    MapEditorComponent,
    KeysPipePipe
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
