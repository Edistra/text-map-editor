import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'keysPipe'
})
export class KeysPipePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const keys = [];
    for (const key of Object.keys(value)) {
      // keys.push({key, value: value[key]});
      keys.push(key);
    }
    return keys;
  }

}
